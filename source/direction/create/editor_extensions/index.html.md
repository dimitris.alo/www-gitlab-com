---
layout: markdown_page
title: "Category Direction - Editor Extensions"
---

- TOC
{:toc}

## Editor Extensions

|                       |                                  |
|-----------------------|----------------------------------|
| Stage                 | [Create](/direction/dev/#create) |
| Content Last Reviewed | `2023-05-17`                     |

### Introduction and how you can help

Thanks for visiting this direction page on Editor Extensions. This page belongs to the [Code Review](/handbook/product/categories/#code-review-group) group of the Create stage and is maintained by Kai Armstrong ([E-Mail](mailto:karmstrong@gitlab.com)).

This direction is constantly evolving and everyone can contribute:

- Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=closed_at_desc&state=opened&label_name%5B%5D=Category%3AEditor%20Extensions&first_page_size=100) and epics on this page. Sharing your feedback directly on GitLab.com  or submitting a Merge Request to this page are the best ways to contribute to our strategy.
- Please share feedback directly via email, Twitter, or [schedule a video call](https://calendly.com/gitlabkai).

### Strategy and Themes
<!-- Describe your category. Capture the main problems to be solved in market (themes). Describe how you intend to solve these with GitLab (strategy). Provide enough context that someone unfamiliar with the details of the category can understand what is being discussed. -->

GitLab supports teams collaborating and building software together, however that collaboration is only available inside the GitLab application.

Developers, on the other hand, spend the majority of their time working locally implementing work outlined in issues, responding to merge request feedback and testing/debugging their applications. These tasks are the core of the developer experience, and GitLab should support developers closer to where they're doing their meaningful work to enable them to be more efficient in the delivery of that work.

### 1 year plan
<!--
1 year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road-map". Items in this section should be linked to issues or epics that are up to date. Indicate relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them. 
 -->

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to giant epics that lack clarity on what is next. -->

Once we've completed efforts to bring GitLab extensions to JetBrains and Visual Studio in support of Code Suggestions, we'll begin looking at other opportunities to expand the capabilities of the extensions in to the rest of the GitLab DevSecOps platform.

#### What we are currently working on

<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

The Editor Extensions category is focused on two primary work streams:

1. Delivering an extension for **JetBrains IDE** supporting [Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html)
1. Delivering an extension for **Visual Studio** supporting [Code Suggestion](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html)

<!-- #### What we recently completed -->
<!-- Lookback limited to 3 months. -->

#### What is Not Planned Right Now
<!--  Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand -->

The scope of this category does not extend beyond extension support and foundations for:

- JetBrains IDE's
- Visual Studio

Support for Visual Studio Code remains with the [IDE group](https://about.gitlab.com/direction/create/ide/) and the [GitLab CLI](https://about.gitlab.com/direction/create/gitlab_cli/) remains it's own category.

<!-- ### Target Audience -->
<!--
List the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->

<!-- 
The software development process involves many people working across various parts of configuration, contribution and review. All of these users work together to advance software projects in their organization.

Engineering personas who are contributing to development, configuring or interacting with continuous integration and reviewing contributions from other team members. Users performing these tasks need tools that allow them to deeply understand the changes and provide meaningful feedback of both comments and code suggestions. These are specifically addressed by the following GitLab Personas:

- [Sasha (Software Developer)](/handbook/product/personas/#sasha-software-developer)
- [Devon (DevOps Engineer)](/handbook/product/personas/#devon-devops-engineer)
-->
